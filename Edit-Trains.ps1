param (
	[string]$DSFToolPath=$(throw "-DSFToolPath is required.")
)

function editDSF {
	param (
		[string]$dsfPath=$(throw "-dsfPath is required.")
	)
	
	if (-not (Test-Path -Path $dsfPath)) {
		Write-Error "Skipping $dsfPath. Path not found."
		return 1
	}
	
	#convert dsf to text
	& $DSFToolPath --dsf2text $dsfPath tempDSF.txt
	if ($LastExitCode -gt 0) {
		Write-Error "Skipping $dsfPath. Conversion to txt failed."
		return 1
	}
	
	if (-not (Select-String -Pattern "network/roads_CZ.net" -Path tempDSF.txt -SimpleMatch -Quiet)) {	
		#edit the textfile
		(((Get-Content -path tempDSF.txt -Raw) -replace 'lib/g10/roads_EU.net','network/roads_CZ.net') | Set-Content -Path tempDSF.txt)
		
		#convert back to dsf
		& $DSFToolPath --text2dsf tempDSF.txt $dsfPath
		if ($LastExitCode -gt 0) {
			Write-Error "Skipping $dsfPath. Conversion to dsf failed."
			return 1
		}
		Echo "Edited Successfully. $dsfPath"
	} else {
		Echo "Skipping... $dsfPath already edited."
	}
	#clean up
	(Remove-Item tempDSF.txt)
	
	return 0
}

#check DSFToolPath for existence
if (-not (Test-Path -Path $DSFToolPath)) {
	throw 'Incorrect -DSFToolPath parameter.'
}

#test we run from correct place
if (-not (Test-Path -Path "..\..\Resources\default scenery\1000 roads\roads_EU.net")) {
	throw "It must be run from Custom Scenery\simHeaven_X-Europe-3-network. Stopping now."
}

#create new subdirectory and symbolic links
if (-not (Test-Path -Path "network")) {
	$result=New-Item -ItemType directory -Path network
	if (-not $result) {
		throw "Can't create network folder. Stopping."
	}
	Echo "network folder created."
} else {
	Echo "network folder found."
}

#copy default roads_EU.net to the scenery, so that user can edit it without irritating X-Plane updater
if (-not (Test-Path -Path "network\roads_EU.net")) {
	Copy-Item "..\..\Resources\default scenery\1000 roads\roads_EU.net" network\roads_CZ.net
	Echo "Default roads_EU.net copied to roads_CZ in network folder."
} else {
	Echo "roads_CZ.net found in network folder. Leaving as-is."
}
	
#create symbolic links for scenery load
if (-not (Test-Path -Path "network\objects")) {
	$result=new-item -itemtype symboliclink -path network -name objects -value "..\..\Resources\default scenery\1000 roads\objects"
	if (-not $result) {
		throw "Can't create symbolic link for objects. Stopping."
	}
	Echo "Symbolic link for ..\..\Resources\default scenery\1000 roads\objects created in network folder."
} else {
	Echo "Symbolic link for objects found in network folder."
}

if (-not (Test-Path -Path "network\textures")) {
	$result=new-item -itemtype symboliclink -path network -name textures -value "..\..\Resources\default scenery\1000 roads\textures"
	if (-not $result) {
		throw "Can't create symbolic link for textures. Stopping."
	}
	Echo "Symbolic link for ..\..\Resources\default scenery\1000 roads\textures created in network folder."
} else {
	Echo "Symbolic link for textures found in network folder."
}
	
[string[]]$dsfList=@(
	"Earth nav data/+40+010/+48+013.dsf", #CZ
	"Earth nav data/+40+010/+48+014.dsf",
	"Earth nav data/+40+010/+48+015.dsf",
	"Earth nav data/+40+010/+48+016.dsf",
	"Earth nav data/+40+010/+48+017.dsf",
	"Earth nav data/+40+010/+49+012.dsf",
	"Earth nav data/+40+010/+49+013.dsf",
	"Earth nav data/+40+010/+49+014.dsf",
	"Earth nav data/+40+010/+49+015.dsf",
	"Earth nav data/+40+010/+49+016.dsf",
	"Earth nav data/+40+010/+49+017.dsf",
	"Earth nav data/+40+010/+49+018.dsf",
	"Earth nav data/+50+010/+50+012.dsf",
	"Earth nav data/+50+010/+50+013.dsf",
	"Earth nav data/+50+010/+50+014.dsf",
	"Earth nav data/+50+010/+50+015.dsf",
	"Earth nav data/+50+010/+50+016.dsf",
	"Earth nav data/+50+010/+50+017.dsf",
	"Earth nav data/+40+010/+47+017.dsf", #SK
	"Earth nav data/+40+010/+47+018.dsf",
	"Earth nav data/+40+010/+48+016.dsf",
	"Earth nav data/+40+010/+48+017.dsf",
	"Earth nav data/+40+010/+48+018.dsf",
	"Earth nav data/+40+010/+48+019.dsf",
	"Earth nav data/+40+010/+49+017.dsf",
	"Earth nav data/+40+010/+49+018.dsf",
	"Earth nav data/+40+010/+49+019.dsf",
	"Earth nav data/+40+020/+48+020.dsf",
	"Earth nav data/+40+020/+48+021.dsf",
	"Earth nav data/+40+020/+48+022.dsf",
	"Earth nav data/+40+020/+49+020.dsf",
	"Earth nav data/+40+020/+49+021.dsf",
	"Earth nav data/+40+020/+49+022.dsf"
	
)

foreach ($dsf in $dsfList) {
	editDSF -dsfPath $dsf
}

Echo "Script finished."