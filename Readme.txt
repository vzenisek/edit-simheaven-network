Navod k pouziti:

K cemu to je? Script je urceny k editaci simHeaven_X-Europe-3-network.
Prepise vam to reference na defnici networku v cr a sr, abyste nasledne mohli tuto definici upravit primo ve scenerce
a XPL updater to nechalo chladnym.
Tim padem vam budou ceske masinky jezdit jen v Cechach a na Slovensku.

Drive nez zacnete script pouzivat, prectete si tento navod a bezpecnostni instrukce :-D

Pokud jste nikdy nepousteli zadny powershelovy script:
- spustte powershell jako administrator.
- Nakopirujte tam a odentrujte nasledujici: 

Set-ExecutionPolicy RemoteSigned

Dal postupujte podle navodu:

1) Stahnete si XPTools z http://dev.x-plane.com/download/tools/xptools_win_15-3.zip a nekam si to rozbalte
2) Rozbalte script Edit-Trains.ps1 do Custom Scenery\simHeaven_X-Europe-3-network\Edit-Trains.ps1
   Kdyz se to spusti od jinud, nemelo by to nic zkurvit, ale taky to nic neudela, krome toho, ze to vybleje spoustu cervenejch hlasek.
3) Spustte si powershell jako administrator v ceste Custom Scenery\simHeaven_X-Europe-3-network\
4) Spustte script takto:

.\Edit-Trains.ps1 <Plna cesta k vasemu DSFTool.exe>

Priklad: .\Edit-Trains.ps1 g:\Vita\SimData\XPlaneFiles\Utils\DSFTool\DSFTool.exe

5) Chvilu to pobezi. Meli byste videt sama bila pismenka na modrem pozadi. Vidite-li cervena, je neco spatne. Napiste mi, co.

Az to dobehne, tak zjistite, ze se vam vedle scriptu objevil folder network, v nem roads_CZ.net a dva symbolicke linky (objects a textures).
roads_CZ.net je kopie vzata z Resources\default scenery\1000 roads\roads_EU.net 

6) Ted nastavaji 2 moznosti:
- pokud jste si tento default (Resources\default scenery\1000 roads\roads_EU.net) jiz predtim editovali, jen tu editaci vratte. Pokud jste nemeli backup, 
spravi vam to updater.
- pokud jste jeste editaci neprosli, editujte tento novy (cili Custom Scenery\simHeaven_X-Europe-3-network\network\roads_CZ.net) dle instrukci od leguana
  (neco mi rika, ze je bude brzo updatovat :-))

XPL sotousum zdar!
